package kafkaapp;

import java.time.Duration;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;

public class GatesApp implements ILandingListener {

	public void handleLanding() {

		Properties config = new Properties();
		config.put("key.deserializer", StringDeserializer.class.getName());
		config.put("value.deserializer", StringDeserializer.class.getName());
		config.put("bootstrap.servers", "localhost:9092");
		config.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer1");
		Consumer<String, String> consumer = new KafkaConsumer<String, String>(config);
		consumer.subscribe(Arrays.asList("RUNSWAYS1"));
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(5));
			for (ConsumerRecord<String, String> record : records) {
				System.out.println(record.key());
				System.out.println(record.value());
			}
		}
	}

	public static void main(String[] args) {

		new GatesApp().handleLanding();
	}
}
