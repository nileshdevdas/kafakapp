package kafkaapp;

import java.util.Properties;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;

public class KafkaStreamDemo {

	public static void main(String[] args) {

		Properties config = new Properties();
		config.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamer");
		config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		config.put("key.serializer", StringSerializer.class.getName());
		config.put("key.deserializer", StringDeserializer.class.getName());

		StreamsBuilder builder = new StreamsBuilder();

		builder.stream("RUNSWAYS").filter((a, b) -> {
			// filteration
			return true;
		}).to("RUNWAY1");

		KafkaStreams streams = new KafkaStreams(builder.build(), config);
		streams.cleanUp();
		streams.start();
	}
}
