package kafkaapp;

import java.util.Timer;
import java.util.TimerTask;

public class RunwayLandingTimer {

	public static void main(String[] args) {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new LandingTask(), 500, 5000);
	}

	private static class LandingTask extends TimerTask {
		@Override
		public void run() {
			System.out.println("Landed ....");
		}
	}
}
