package kafkaapp;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaApp {

	public static void main(String[] args) {
		Properties config = new Properties();
		config.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		config.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		config.put("bootstrap.servers", "localhost:9092");
		Producer<String, String> producer = new KafkaProducer<String, String>(config);
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < 1000000; i++) {
			ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>("RUNSWAYS1", "message",
					"DS001 Has Landed");
			producer.send(producerRecord);

		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total Time " + (endTime - startTime));
		producer.close();
	}

}
